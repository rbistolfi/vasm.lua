========
VASM.lua
========


Toolkit for writing VASM applications in Lua.

Overview
--------

::

    vasm = require "vasm"
    model = vasm.model


    -- Create a controller
    UserController = vasm.Controller:new()

    -- define a render() function for telling what to show
    function UserController:render()
        -- models are rendered later using the view you setup
        self.description = model.SimpleText:new {text = "Welcome, luser"}
        self.username = model.TextInput:new {caption = "Enter your name"}
        self.lastname = model.TextInput:new {caption = "Enter your lastname"}
        return {
            self.description, 
            self.username,
            self.lastname
        }
    end

    -- define a command() function for processing user input
    function UserController:command(username, lastname)
        -- this will be called using the data from the user
        print("Your name is " .. username)
        print("Your last name is " .. lastname)
        -- models are observed, window is updated
        self.description:set_text("Thanks, " .. username)
    end

    -- the app object runs the thing
    app = vasm.Application:new()
    app:set_view("gtk")
    app:run(UserController)

This shows a dialog with a label for rendering the description and two text
entry widgets for username and lastname. If user clicks "OK", the **command()**
function is called using the input as arguments.


Status
------

Experimental, nothing is ready.


Testing
-------

You can run *lua example.lua* in the *src* folder. You need *lgi*, the Lua
bindings for gobject, and GTK+3. 
