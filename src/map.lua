-- map.lua
-- Map models to views
-- rbistolfi


require("object")


local Registry = Object:new {registry = {}} 


function Registry:set(model, view)
    -- declare that a view renders model
    self.registry[model] = view
end


function Registry:get(model)
    -- get a view for the model
    return self.registry[model]
end


local function render(model)
    -- widget factory
    -- create a widget for the model
    -- local model_class = getmetatable(model)
    local model_class = model:kind()
    local view = r:get(model_class):new()
    view:set_model(model)
    model.delegate = view
    return view
end


-- singleton
r = Registry:new()


map = {}
map.get = function(model) return r:get(model) end
map.set = function(model, view) return r:set(model, view) end
map.render = render
return map
