-- example.lua
-- example of VASM framework
-- rbistolfi


vasm = require("vasm")
model = vasm.model


-- Create controller object
Example = vasm.Controller:new()


function Example:render()
    -- use the render function to tell us what to show in the screen
    -- each model will be rendered using the view
    -- models are observable, modify them and the view will be updated
    self.count = 0
    self.text = model.SimpleText:new {text = "Hello World!"}
    return {
        model.SimpleText:new {text= "This is an example"},
        self.text,
	model.TextInput:new {text = "", caption = "Enter text"}
    }
end


function Example:command(input)
    -- use the command function to react to user input
    -- this will be called with the values user entered in the view
    -- the args are one for each model you returned from render if it takes user input
    -- here input is from the TextInput, since SimpleText doesnt accept input
    self.count = self.count + 1
    self.text:set_text("Hey! You clicked " .. self.count .. " times")
    print("You said " .. input)
end


-- the app object knows how to run your controller. Use set_view() to setup the view
app = vasm.Application:new()
app:set_view("gtk")
app:run(Example)
