-- model.lua
-- Model object for VASM
-- rbistolfi


-- Model - Data abstraction layer


require("object")


local Model = Object:new {delegate = nil} 


function Model:notify()
    -- Notify delegate
    if self.delegate then
        self.delegate:on_model_changed(self)
    end
end


local SimpleText = Model:new {text = "", delegate = nil}


function SimpleText:set_text(text)
    -- set text property and notify delegate
    self.text = text
    self:notify()
end


local TextInput = Model:new {text = "", caption = "", delegate = nil}


function TextInput:set_text(text)
    -- set text property and notify delegate
    self.text = text
    self:notify()
end


model = {}
model.Model = Model
model.SimpleText = SimpleText
model.TextInput = TextInput
return model
