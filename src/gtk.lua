-- gtk.lua
-- gtk view for VASM
-- rbistolfi


local view = require("view")
local model = require("model")
local lgi = require("lgi")
local Gtk = lgi.Gtk


local MainWindow = Object:new()


function MainWindow:setup()
    -- Setup the main window
    self.views = {}
    self.apply = Gtk.Button { use_stock = true, label = Gtk.STOCK_OK }
    self.cancel = Gtk.Button { use_stock = true, label = Gtk.STOCK_CANCEL }
    self.body = Gtk.Box {
        orientation = "VERTICAL",
        border_width = 5
    }
    self.window = Gtk.Window {
        title = "VASM",
        border_width = 10,
        Gtk.Box {
            orientation = "VERTICAL",
            self.body,
            Gtk.ButtonBox {
                orientation = "HORIZONTAL",
                border_width = 5,
                layout_style = "END",
                spacing = 10,
                self.apply,
                self.cancel
            }
        }
    }
    function self.window:on_destroy() Gtk.main_quit() end
end


function MainWindow:set_body(views)
    -- Set the contents of the window
    for i, v in ipairs(views) do
        self.body:pack_start(v:widget(), false, false, 5)
	self.views[i] = v
    end
end


function MainWindow:get_user_input()
    -- Collect the state of each widget
    inputs = {}
    for i, v in ipairs(self.views) do
        if v then
            inputs[#inputs + 1] = v:get_user_input()
        end
    end
    return inputs
end


function MainWindow:on_apply_clicked(func)
    -- Connect func to the clicked event of the apply button
    function self.apply:on_clicked(event)
        func()
    end
end


function MainWindow:on_cancel_clicked(func)
    -- Connect func to the clicked event of the cancel button
    function self.cancel:on_clicked(event)
        func()
    end
end


function MainWindow:run()
    -- Start the event loop
    self.window:show_all()
    Gtk.main()
end


function MainWindow:quit()
    -- Exit the event loop
    Gtk.main_quit()
end


local GtkText = view.View:new()
GtkText:renders(model.SimpleText)


function GtkText:set_model(model)
    -- store model and create widget for it
    self.model = model
    self._widget = Gtk.Label {label = model.text}
end


function GtkText:widget()
    -- Configure and return the widget
    return self._widget
end


function GtkText:on_model_changed(model)
    -- react to model change
    self._widget:set_text(model.text)
end


local GtkTextInput = view.View:new()
GtkTextInput:renders(model.TextInput)


function GtkTextInput:set_model(model)
    self.caption = Gtk.Label {label = model.caption}
    self.entry = Gtk.Entry()
    self.entry:set_text(model.text)
    self._widget = Gtk.Box {
       orientation = "HORIZONTAL",
       spacing = 10,
       self.caption,
       self.entry
    }
end


function GtkTextInput:widget()
   return self._widget
end


function GtkTextInput:on_model_changed(model)
   self.caption:set_text(model.caption)
   self.entry:set_text(model.text)
end


function GtkTextInput:get_user_input()
   return self.entry:get_text()
end


return {
    MainWindow = MainWindow,
    GtkText = GtkText,
    GtkTextInput = GtkTextInput
}
