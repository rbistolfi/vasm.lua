-- vasm.lua
-- VASM main module
-- rbistolfi


model = require("model")
view = require("view")
controller = require("controller")
map = require("map")
app = require("app")

require("object")

return {
   model = model,
   view = view,
   controller = controller,
   map = map,
   app = app,
   Model = model.Model,
   View = view.View,
   Application = app.Application,
   Controller = controller.Controller,
   Object = Object
}
