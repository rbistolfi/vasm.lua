-- controller.lua
-- Controller module for VASM
-- rbistolfi


require("object")


local Controller = Object:new()


function Controller:render()
    -- Return a render object
end


function Controller:command()
    -- Handle user input here
end


return {
    Controller = Controller
}
