-- object.lua
-- Base object for VASM
-- rbistolfi


Object = {}


function Object:new(obj)
    -- Create a new instance
    local o = obj or {}
    setmetatable(o, self)
    self.__index = self
    return o
end


function Object:kind()
    -- Return the parent object
    return getmetatable(self)
end
