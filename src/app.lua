-- app.lua
-- Application for VASM
-- rbistolfi


require("object")
local map = require("map")


local DialogProtocol = Object:new {controller = nil, view = nil}


function DialogProtocol:show()
    -- Render the data for the user
    local widgets = self:render_widgets()
    self.main_window:set_body(widgets)
    self.main_window:on_apply_clicked(function() self:on_apply_clicked() end)
    self.main_window:on_cancel_clicked(function() self:on_cancel_clicked() end)
end


function DialogProtocol:render_widgets()
    -- Take the models provided by controller and create
    -- widgets for them
    models = self.controller:render()
    local widgets = {}
    for i, model in ipairs(models) do
        widgets[i] = map.render(model)
    end
    return widgets
end


function DialogProtocol:on_apply_clicked()
    -- Get user input and pass it to the controller
    local user_input = self.main_window:get_user_input()
    self.controller:command(unpack(user_input))
end


function DialogProtocol:on_cancel_clicked()
    -- Executed when Cancel button is clicked
    self.main_window:quit()
end


local Application = Object:new {protocol = DialogProtocol:new()}


function Application:set_view(v)
    -- Setup the view layer for this app
    self.view = require(v)
    self.protocol.main_window = self.view.MainWindow:new()
    self.protocol.main_window:setup()
end


function Application:run(controller)
    -- Run the controller
    self.protocol.controller = controller
    self.protocol:show()
    self.protocol.main_window:run()
end


return {
    Application = Application
}
