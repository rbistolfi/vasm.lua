-- view.lua
-- View module for VASM
-- rbistolfi


require("object")
local map = require("map")


local View = Object:new()


function View:renders(model)
    -- Declare that this view renders model
    map.set(model, self)
end


function View:on_model_changed(model)
    -- Default implementation does nothing
end


function View:get_user_input()
    -- Return the state of the widget
    -- By default returns nil, meaning that widget doesnt accept user input
    return nil
end


view = {}
view.View = View
return view
